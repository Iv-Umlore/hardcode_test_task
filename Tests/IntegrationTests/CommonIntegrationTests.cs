﻿using DbConnections.Cache;
using DbConnections.Interfaces;
using DbConnections.Models;
using DbConnections.Realization;
using FluentAssertions;
using NUnit.Framework;
using Services.Interfaces;
using Services.Realization;

namespace Tests.IntegrationTests
{
    [TestFixture]
    public class CommonIntegrationTests : DbBaseFunc
    {
        private ICommonService _commonService;

        [SetUp]
        public void SetUp()
        {
            dbName = "common.db";
            DeleteDbFileIfExist();
            _context = new DbTables(new SQLite.SQLiteConnectionString(dbName));

            IDbFieldsInteraction fieldsInteraction = new DbFieldsInteraction(_context, new FieldCache());
            IDbCategoryFieldsInteraction dbCategoryFieldInteraction = new DbCategoryFieldsInteraction(_context);
            IDbProductFieldsInteraction dbProductFieldsInteraction = new DbProductFieldsInteraction(_context);
            _commonService = new CommonService(fieldsInteraction, dbCategoryFieldInteraction, dbProductFieldsInteraction);
        }

        [TearDown]
        public void End()
        {
            DeleteDbFileIfExist();
        }

        [TestCase]
        public void GetFields_Empty_Test()
        {
            // Arrange
            int expectedCount = 0;

            // Act
            var fList = _commonService.GetFields();

            // Assert
            fList.Count().Should().Be(expectedCount);
        }

        [TestCase]
        public void GetFields_Test()
        {
            // Arrange
            int expectedCount = 4;

            _context.InsertFields(new List<Field>()
            {
                new Field() { Name = "suawq2" },
                new Field() { Name = "kdisds14t" },
                new Field() { Name = "abYt" },
                new Field() { Name = "549dkfsdw5" }
            });

            // Act
            var fList = _commonService.GetFields();

            // Assert
            fList.Count().Should().Be(expectedCount);
        }

        [TestCase]
        public void FindFieldByName_Test()
        {
            // Arrange
            string name = "abYt";
            int expectedCount = 3;
            string expectedName = "Abyt";

            _context.InsertFields(new List<Field>()
            {
                new Field() { Name = "suawq2" },
                new Field() { Name = "kdisds14t" },
                new Field() { Name = "abYt" },
                new Field() { Name = "549dkfsdw5" }
            });
            
            // Act
            var field = _commonService.FindFieldByName(name);

            // Assert
            field.Name.Should().Be(expectedName);
            field.Id.Should().Be(expectedCount);
        }

        [TestCase]
        public void GetCategoryField_Test()
        {
            // Arrange

            int expectedCount = 1;
            string expectedName = "Kdisds14t";


            _context.InsertFields(new List<Field>()
            {
                new Field() { Name = "suawq2" },
                new Field() { Name = "kDisDs14T" }
            });

            int id = _context.InsertCategory(new Category() { Name = "BestCat" });
            _context.InsertCategoryFields(id, new List<int>() { 2 });

            // Act
            var fields = _commonService.GetCategoryField(id);

            // Assert
            fields.Count.Should().Be(expectedCount);
            fields.First().Name.Should().Be(expectedName);
        }

        [TestCase]
        public void GetProductField_Test()
        {
            // Arrange

            int expectedCount = 1;
            string expectedName = "Kdisds14t";


            _context.InsertFields(new List<Field>()
            {
                new Field() { Name = "suawq2" },
                new Field() { Name = "kDisDs14T" }
            });

            var fieldId = _context.Fields.First(it => it.Name == "Kdisds14t").Id;

            int id = _context.InsertProduct(new Product() { Name = "BestProd" });
            _context.InsertProductField(new ProductField() { FieldId = fieldId, ProductId = id, Value = "CategoryValue" });

            // Act
            var fields = _commonService.GetProductField(id);

            // Assert
            fields.Count.Should().Be(expectedCount);
            fields.First().Name.Should().Be(expectedName);
        }

        [TestCase]
        public void GetProductFields_Few_Test()
        {
            // Arrange

            int expectedCount = 2;
            string expectedName = "Kdisds14t";
            string secondExpected = "164";

            _context.InsertFields(new List<Field>()
            {
                new Field() { Name = "suawq2" },
                new Field() { Name = "kDisDs14T" },
                new Field() { Name = "164" },
                new Field() { Name = "Second" }
            });

            var fieldId = _context.Fields.First(it => it.Name == "Kdisds14t").Id;
            var fieldId2 = _context.Fields.First(it => it.Name == "164").Id;

            int id = _context.InsertProduct(new Product() { Name = "BestProd" });
            _context.InsertProductField(new ProductField() { FieldId = fieldId, ProductId = id, Value = "CategoryValue" });
            _context.InsertProductField(new ProductField() { FieldId = fieldId2, ProductId = id, Value = "Val" });

            // Act
            var fields = _commonService.GetProductField(id);

            // Assert
            fields.Count.Should().Be(expectedCount);
            fields.First().Name.Should().Be(expectedName);
            fields.Last().Name.Should().Be(secondExpected);
        }
    }
}
