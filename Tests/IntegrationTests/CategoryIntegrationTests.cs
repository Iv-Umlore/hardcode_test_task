﻿using Common.Models.InputModels;
using DbConnections.Cache;
using DbConnections.Interfaces;
using DbConnections.Models;
using DbConnections.Realization;
using FluentAssertions;
using NUnit.Framework;
using Services.Interfaces;
using Services.Realization;

namespace Tests.IntegrationTests
{
    [TestFixture]
    public class CategoryIntegrationTests : DbBaseFunc
    {
        private ICategoryService _categoryService;

        [SetUp]
        public void SetUp()
        {
            dbName = "category.db";
            DeleteDbFileIfExist();
            _context = new DbTables(new SQLite.SQLiteConnectionString(dbName));
            
            IDbProductInteraction dbProductInteraction = new DbProductInteraction(_context);
            IDbProductFieldsInteraction dbProductFieldsInteraction = new DbProductFieldsInteraction(_context);

            IDbCategoryInteraction dbCategoryInteraction = new DbCategoryInteraction(_context);
            IDbFieldsInteraction fieldsInteraction = new DbFieldsInteraction(_context, new FieldCache());
            IDbCategoryFieldsInteraction dbCategoryFieldInteraction = new DbCategoryFieldsInteraction(_context);

            ICommonService commonService = new CommonService(fieldsInteraction, dbCategoryFieldInteraction, dbProductFieldsInteraction);
            IProductService productService = new ProductService(dbProductInteraction, fieldsInteraction, dbProductFieldsInteraction, commonService);

            _categoryService = new CategoryService(dbCategoryInteraction, fieldsInteraction, dbCategoryFieldInteraction, productService);
        }

        [TearDown]
        public void End()
        {
            DeleteDbFileIfExist();
        }

        [TestCase]
        public void GetCategories_Tests()
        {
            // Arrange
            _context.InsertCategory(firstCat);
            _context.InsertCategory(secondCat);
            _context.InsertCategory(thirdCat);
            _context.InsertCategory(fourthCat);

            int expectedCount = 4;

            // Act
            var categories = _categoryService.GetCategories();

            // Assert
            categories.Count().Should().Be(expectedCount);

            categories[0].Name.Should().Be(firstCat.Name);
            categories[1].Name.Should().Be(secondCat.Name);
            categories[2].Name.Should().Be(thirdCat.Name);
            categories[3].Name.Should().Be(fourthCat.Name);
        }

        [TestCase]
        public void GetCategory_Tests()
        {
            // Arrange
            _context.InsertCategory(firstCat);
            _context.InsertCategory(secondCat);
            _context.InsertCategory(thirdCat);
            _context.InsertCategory(fourthCat);

            // Act
            var category = _categoryService.GetCategory(2);

            // Assert
            category.Name.Should().Be(secondCat.Name);
            category.Id.Should().Be(2);
        }

        [TestCase]
        public void AddCategory_Tests()
        {
            // Arrange
            _context.InsertCategory(firstCat);
            _context.InsertCategory(secondCat);
            _context.InsertCategory(thirdCat);
            _context.InsertCategory(fourthCat);

            _context.InsertFields(new List<Field>() { fField, sField, tField });

            var expectedName = "New category";
            var expectedCategoryFieldCount = 2;
            var expectedFieldCount = 3;

            // Act
            _categoryService.AddCategory(new CategoryInputModel()
            {
                Name = "New Category",
                Fields = new List<string>() { fField.Name, tField.Name.ToLower() }
            });

            // Assert
            var category = _categoryService.GetCategory(5);

            _context.Fields.Count().Should().Be(expectedFieldCount);

            category.Name.Should().Be(expectedName);
            category.Fields.Should().NotBeNull();
            category.Fields.Count().Should().Be(expectedCategoryFieldCount);

            category.Id.Should().Be(5);
            category.Fields[fField.Id].Should().Be(fField.Name);
            category.Fields[tField.Id].Should().Be(tField.Name);
        }

        [TestCase]
        public void AddCategory_WithoutFields_Tests()
        {
            // Arrange
            _context.InsertCategory(firstCat);
            _context.InsertCategory(secondCat);
            _context.InsertCategory(thirdCat);
            _context.InsertCategory(fourthCat);

            _context.InsertFields(new List<Field>() { fField, sField, tField });

            var expectedName = "New category";
            var expectedFieldCount = 0;

            // Act
            _categoryService.AddCategory(new CategoryInputModel()
            {
                Name = "New Category"
            });

            // Assert
            var category = _categoryService.GetCategory(5);

            category.Name.Should().Be(expectedName);
            category.Fields.Should().NotBeNull();
            category.Fields.Count().Should().Be(expectedFieldCount);
        }

        [TestCase]
        public void RemoveCategory_WithoutFields_Tests()
        {
            // Arrange
            _context.InsertCategory(firstCat);
            _context.InsertCategory(secondCat);

            _categoryService.AddCategory(new CategoryInputModel()
            {
                Name = "New Category"
            });

            _context.InsertCategory(thirdCat);
            _context.InsertCategory(fourthCat);


            // Act
            _categoryService.RemoveCategory(3);

            // Assert
            var list = _context.Categories.Where(it => it.Name == "New Category" || it.Name == "New category").ToList();
            list.Count().Should().Be(0);
        }

        [TestCase]
        public void RemoveCategory_WithFields_Tests()
        {

            _context.InsertFields(new List<Field>() { fField, sField, tField });

            // Arrange
            _context.InsertCategory(firstCat);
            _context.InsertCategory(secondCat);

            _categoryService.AddCategory(new CategoryInputModel()
            {
                Name = "New Category",
                Fields = new List<string>() { fField.Name, sField.Name }
            });

            _context.InsertCategory(thirdCat);
            _context.InsertCategory(fourthCat);


            // Act
            _categoryService.RemoveCategory(3);

            // Assert
            var list = _context.CategoryFieldRelationships.Where(it => it.CategoryId == 3).ToList();
            list.Count().Should().Be(0);

            var catList = _context.Categories.Where(it => it.Name == "New Category" || it.Name == "New category").ToList();
            catList.Count.Should().Be(0);

            var fieldList = _context.Fields.Where(it => it.Name == fField.Name || it.Name == sField.Name).ToList();
            fieldList.Count().Should().Be(2);
        }


        #region Data

        private Category firstCat = new Category() { Name = "Категория 1" };
        private Category secondCat = new Category() { Name = "Категория 2" };
        private Category thirdCat = new Category() { Name = "Категория 3" };
        private Category fourthCat = new Category() { Name = "Категория 4" };

        private Field fField = new Field() { Name = "Поле категории 1" };
        private Field sField = new Field() { Name = "Поле категории 2" };
        private Field tField = new Field() { Name = "Поле категории 3" };

        #endregion
    }
}
