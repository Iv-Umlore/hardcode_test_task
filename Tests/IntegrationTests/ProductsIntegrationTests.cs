﻿using Common.Models.InputModels;
using DbConnections.Cache;
using DbConnections.Interfaces;
using DbConnections.Models;
using DbConnections.Realization;
using FluentAssertions;
using NUnit.Framework;
using Services.Interfaces;
using Services.Realization;
using System.Runtime.ConstrainedExecution;

namespace Tests.IntegrationTests
{
    [TestFixture]
    public class ProductsIntegrationTests : DbBaseFunc
    {
        private IProductService _productService;

        [SetUp]
        public void SetUp()
        {
            dbName = "product.db";
            DeleteDbFileIfExist();
            _context = new DbTables(new SQLite.SQLiteConnectionString(dbName));

            IDbFieldsInteraction fieldsInteraction = new DbFieldsInteraction(_context, new FieldCache());
            IDbProductInteraction dbProductInteraction = new DbProductInteraction(_context);
            IDbProductFieldsInteraction dbProductFieldsInteraction = new DbProductFieldsInteraction(_context);
            IDbCategoryFieldsInteraction dbCategoryFieldInteraction = new DbCategoryFieldsInteraction(_context);

            ICommonService commonService = new CommonService(fieldsInteraction, dbCategoryFieldInteraction, dbProductFieldsInteraction);

            _productService = new ProductService(dbProductInteraction, fieldsInteraction, dbProductFieldsInteraction, commonService);
        }

        [TearDown]
        public void End()
        {
            DeleteDbFileIfExist();
        }


        [TestCase]
        public void GetProduct_Tests()
        {
            // Arrange
            var expectedProduct = new Product() {
                Name = "Product",
                Description = "Descr",
                CategoryId = 1,
                Price = 10.89
            };

            _context.InsertProduct(expectedProduct);

            // Act
            var recivedProd = _productService.GetProduct(1);

            // Assert
            recivedProd.CategoryId.Should().Be(expectedProduct.CategoryId);
            recivedProd.Name.Should().Be(expectedProduct.Name);
            recivedProd.Description.Should().Be(expectedProduct.Description);
            recivedProd.Price.Should().Be(expectedProduct.Price);
            recivedProd.Id.Should().Be(1);
            recivedProd.Fields.Count().Should().Be(0);
        }

        [TestCase]
        public void AddProduct_WithoutFields_Test()
        {
            _context.InsertProduct(first);

            ProductInputModel productInputModel = new ProductInputModel()
            {
                Name = "Product 1",
                CategoryId = 1,
                Description = "Top Secret",
                Price = 999.0
            };

            // Act
            Assert.DoesNotThrow(() => _productService.AddProduct(productInputModel));
        }

        [TestCase]
        public void AddProduct_WithFields_Test()
        {
            // Arrange
            _context.InsertFields(new List<Field>()
            {
                new Field(){ Name = "Param 1"},
                new Field(){ Name = "Param 2"},
                new Field(){ Name = "Param 3"},
                new Field(){ Name = "Param 4"}

            });

            double expectedPrice = 999.0;
            string expectedDescription = "Top Secret";
            int expectedCategoryId = 1;
            string expectedName = "Product 1";
            int expectedParamCount = 3;

            ProductInputModel productInputModel = new ProductInputModel()
            {
                Name = "Product 1",
                CategoryId = 1,
                Description = "Top Secret",
                Price = 999.0,
                Fields = new Dictionary<string, string>(new List<KeyValuePair<string, string>>()
                {
                    new KeyValuePair<string, string>("Param 1", "Level 4"),
                    new KeyValuePair<string, string>("Param 2", "Human"),
                    new KeyValuePair<string, string>("Param 3", "extremely dangerous")
                })
            };

            // Act
            _productService.AddProduct(productInputModel);

            var product = _productService.GetProduct(1);

            product.Price.Should().Be(expectedPrice);
            product.CategoryId.Should().Be(expectedCategoryId);
            product.Description.Should().Be(expectedDescription);
            product.Name.Should().Be(expectedName);
            product.Fields.Count.Should().Be(expectedParamCount);
        }

        [TestCase]
        public void AddSecondProduct_Tests()
        {
            // Arrange
            _context.InsertFields(new List<Field>()
            {
                new Field(){ Name = "Param 1"},
                new Field(){ Name = "Param 2"},
                new Field(){ Name = "Param 3"}

            });

            _context.InsertProduct(first);

            ProductInputModel productInputModel = new ProductInputModel()
            {
                Name = "Product 1",
                CategoryId = 1,
                Description = "Top Secret",
                Price = 999.0,
                Fields = new Dictionary<string, string>(new List<KeyValuePair<string, string>>()
                {
                    new KeyValuePair<string, string>("Param 1", "Level 4"),
                    new KeyValuePair<string, string>("Param 2", "Human"),
                    new KeyValuePair<string, string>("Param 3", "extremely dangerous")
                })
            };

            // Act
            _productService.AddProduct(productInputModel);

            // Arrange
            var prod = _productService.GetProduct(2);

            prod.Fields.Count.Should().Be(3);
            prod.Fields["Param 1"].Should().Be("Level 4");
            prod.Fields["Param 2"].Should().Be("Human");
            prod.Fields["Param 3"].Should().Be("extremely dangerous");
        }

        [TestCase]
        public void AddProduct_UncreatedFields_Tests_ExpectException()
        {
            // Arrange
            _context.InsertFields(new List<Field>()
            {
                new Field(){ Name = "Param 1"},
                new Field(){ Name = "Param 2"},
                new Field(){ Name = "Param 4"}

            });

            string expectedMessage = "Value cannot be null. (Parameter 'Не удалось получить объект поля по имени')";

            ProductInputModel productInputModel = new ProductInputModel()
            {
                Name = "Product 1",
                CategoryId = 1,
                Description = "Top Secret",
                Price = 999.0,
                Fields = new Dictionary<string, string>(new List<KeyValuePair<string, string>>()
                {
                    new KeyValuePair<string, string>("Param 1", "Level 4"),
                    new KeyValuePair<string, string>("Param 2", "Human"),
                    new KeyValuePair<string, string>("Param 3", "extremely dangerous")
                })
            };

            // Act
            var exception = Assert.Catch(() => _productService.AddProduct(productInputModel));

            exception.GetType().Should().Be(typeof(ArgumentNullException));
            exception.Message.Should().Be(expectedMessage);
        }


        [TestCase]
        public void GetProducts_Tests()
        {
            // Arrange
            _context.InsertProduct(first);
            _context.InsertProduct(second);
            _context.InsertProduct(third);
            _context.InsertProduct(fourth);

            var expectedCount = 4;

            // Act
            var products = _productService.GetProducts();

            // Assert
            products.Count().Should().Be(expectedCount);

            products[0].Price.Should().Be(first.Price);
            products[0].Name.Should().Be(first.Name);
            products[0].Description.Should().Be(first.Description);
            products[0].Fields.Should().BeNull();

            products[1].Price.Should().Be(second.Price);
            products[1].Name.Should().Be(second.Name);
            products[1].Description.Should().Be(second.Description);
            products[1].Fields.Should().BeNull();

            products[2].Price.Should().Be(third.Price);
            products[2].Name.Should().Be(third.Name);
            products[2].Description.Should().Be(third.Description);
            products[2].Fields.Should().BeNull();

            products[3].Price.Should().Be(fourth.Price);
            products[3].Name.Should().Be(fourth.Name);
            products[3].Description.Should().Be(fourth.Description);
            products[3].Fields.Should().BeNull();

        }

        [TestCase]
        public void GetProducts_Filtered_ByCategory_Test()
        {
            // Arrange
            _context.InsertProduct(first);
            _context.InsertProduct(second);
            _context.InsertProduct(third);
            _context.InsertProduct(fourth);

            var expectedCount = 2;

            // Act
            var products = _productService.GetProducts(1, new Dictionary<string, string>());

            // Assert
            products.Count().Should().Be(expectedCount);
            products[0].Price.Should().Be(first.Price);
            products[0].CategoryId.Should().Be(1);
            products[1].Fields.Should().BeNull();

            products[1].Price.Should().Be(second.Price);
            products[1].CategoryId.Should().Be(1);
            products[1].Fields.Should().BeNull();
        }

        [TestCase]
        public void GetProducts_Filtered_ByField_Test()
        {
            // Arrange
            _context.InsertProduct(first);
            _context.InsertProduct(second);
            _context.InsertProduct(third);
            _context.InsertProduct(fourth);

            _context.InsertField(fField);
            _context.InsertField(sField);
            _context.InsertField(tField);
            _context.InsertField(fourField);

            _context.InsertProductField(new ProductField() { FieldId = 1, ProductId = 1, Value = "10 meters" });
            _context.InsertProductField(new ProductField() { FieldId = 2, ProductId = 1, Value = "6" });
            _context.InsertProductField(new ProductField() { FieldId = 3, ProductId = 2, Value = "0xPer" });
            _context.InsertProductField(new ProductField() { FieldId = 4, ProductId = 3, Value = "Art" });
            _context.InsertProductField(new ProductField() { FieldId = 4, ProductId = 4, Value = "Nyt" });

            var fieldFilter = new Dictionary<string, string>(new List<KeyValuePair<string, string>>()
            {
                new KeyValuePair<string, string>(fourField.Name, "Art")
            });

            var expectedCount = 1;

            // Act
            var products = _productService.GetProducts(2, fieldFilter);

            // Assert
            products.Count().Should().Be(expectedCount);
            products[0].Price.Should().Be(third.Price);
            products[0].CategoryId.Should().Be(2);

            // Поля не выдаются при обычных запросах
            products[0].Fields.Should().BeNull();
        }


        [TestCase]
        public void RemoveProduct_Tests()
        {
            // Arrange
            _context.InsertProduct(first);
            _context.InsertProduct(second);
            _context.InsertProduct(third);
            _context.InsertProduct(fourth);

            var expectedCountBefore = 4;
            var expectedCountAfter = 3;

            // Act
            var countBefore = _productService.GetProducts().Count();

            _productService.RemoveProduct(1);

            var products = _productService.GetProducts();

            // Assert
            countBefore.Should().Be(expectedCountBefore);
            products.Count().Should().Be(expectedCountAfter);

            products[0].Price.Should().Be(second.Price);
            products[0].Name.Should().Be(second.Name);
            products[0].Description.Should().Be(second.Description);
            products[0].Fields.Should().BeNull();

            products[1].Price.Should().Be(third.Price);
            products[1].Name.Should().Be(third.Name);
            products[1].Description.Should().Be(third.Description);
            products[1].Fields.Should().BeNull();

            products[2].Price.Should().Be(fourth.Price);
            products[2].Name.Should().Be(fourth.Name);
            products[2].Description.Should().Be(fourth.Description);
            products[2].Fields.Should().BeNull();
        }


        #region Data

        private Product first = new Product() {
            Price = 10.89,
            CategoryId = 1,
            Description = "Description",
            Name = "Product 1"
        };

        private Product second = new Product()
        {
            Price = 11.78,
            CategoryId = 1,
            Description = "Des",
            Name = "Product 2"
        };

        private Product third = new Product()
        {
            Price = 8.94,
            CategoryId = 2,
            Description = "",
            Name = "Product 3"
        };

        private Product fourth = new Product()
        {
            Price = 1.34,
            CategoryId = 2,
            Description = "Descr",
            Name = "Car"
        };

        private Field fField = new Field() { Name = "Size" };

        private Field sField = new Field() { Name = "Count" };

        private Field tField = new Field() { Name = "Ux8" };

        private Field fourField = new Field() { Name = "Popit" };

        #endregion
    }
}
