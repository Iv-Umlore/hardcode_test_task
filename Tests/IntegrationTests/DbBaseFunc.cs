﻿using DbConnections.Interfaces;
using DbConnections.Realization;

namespace Tests.IntegrationTests
{
    public abstract class DbBaseFunc
    {
        protected string dbName = "base.db";
        protected DbTablesContext _context;

        protected void DeleteDbFileIfExist()
        {
            FileInfo fi = new FileInfo(dbName);
            try
            {
                if (fi.Exists)
                {
                    if (_context != null)
                        ((DbTables)_context).CloseConnection();
                    fi.Delete();
                }
            }
            catch (Exception)
            {
                fi.Delete();
            }
        }
    }
}
