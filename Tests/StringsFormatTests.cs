﻿using Common.Methods;
using FluentAssertions;
using NUnit.Framework;

namespace Tests
{
    [TestFixture]
    public class StringsFormatTests
    {
        [TestCase]
        public void FormatingName_Test1()
        {
            // Arrange
            string startString = "aaaaaGt";
            string expectedString = "Aaaaagt";

            // Act
            var res = startString.BaseFirstHigh();

            // Assert
            res.Should().Be(expectedString);
        }

        [TestCase]
        public void FormatingName_Test2()
        {
            // Arrange
            string startString = "Fact yup";
            string expectedString = "Fact yup";

            // Act
            var res = startString.BaseFirstHigh();

            // Assert
            res.Should().Be(expectedString);
        }

        [TestCase]
        public void FormatingName_Test3()
        {
            // Arrange
            string startString = "AywdJHslwqsoqckYEL";
            string expectedString = "Aywdjhslwqsoqckyel";

            // Act
            var res = startString.BaseFirstHigh();

            // Assert
            res.Should().Be(expectedString);
        }

        [TestCase]
        public void FormatingName_RusTest_Test4()
        {
            // Arrange
            string startString = "каТегоРия";
            string expectedString = "Категория";

            // Act
            var res = startString.BaseFirstHigh();

            // Assert
            res.Should().Be(expectedString);
        }

        [TestCase]
        public void FormatingName_WithSpecSumbols_Test5()
        {
            // Arrange
            string startString = "BMW_CAR_XXXX#^$%!";
            string expectedString = "Bmw_car_xxxx#^$%!";

            // Act
            var res = startString.BaseFirstHigh();

            // Assert
            res.Should().Be(expectedString);
        }
    }
}
