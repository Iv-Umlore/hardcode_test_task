﻿using Common.Models.OutputModels;
using FluentAssertions;
using NUnit.Framework;
using System.Text.Encodings.Web;
using System.Text.Json;
using System.Text.Unicode;

namespace Tests.SerializationTest
{
    [TestFixture]
    public class FieldOutput_JsonSerialize_Tests
    {
        private JsonSerializerOptions options;

        [SetUp]
        public void CreateOptions()
        {
            options = new JsonSerializerOptions
            {
                Encoder = JavaScriptEncoder.Create(UnicodeRanges.BasicLatin, UnicodeRanges.Cyrillic, UnicodeRanges.Armenian),
                WriteIndented = true
            };
        }

        [TestCase]
        public void FieldSerialization_WithoutFields()
        {
            // Arrange
            FieldOutputModel Field = new FieldOutputModel()
            {
                Id = 4,
                Name = "ticket"
            };
            string expectedResult = "{\"FieldId\":4,\"Name\":\"ticket\"}";

            // Act
            string jsonRes = JsonSerializer.Serialize(Field);

            // Assert
            jsonRes.Should().Be(expectedResult);
        }

        [TestCase]
        public void FieldSerialization_RussianName()
        {
            // Arrange
            FieldOutputModel Field = new FieldOutputModel()
            {
                Id = 4,
                Name = "Билет"
            };
            string expectedResult = "{\"FieldId\":4,\"Name\":\"\\u0411\\u0438\\u043B\\u0435\\u0442\"}";

            // Act
            string jsonRes = JsonSerializer.Serialize(Field);

            // Assert
            jsonRes.Should().Be(expectedResult);
        }

        [TestCase]
        public void FieldSerialization_RussianName_CustomOptions()
        {
            // Arrange
            FieldOutputModel Field = new FieldOutputModel()
            {
                Id = 4,
                Name = "Билет"
            };
            string expectedResult = "{\r\n  \"FieldId\": 4,\r\n  \"Name\": \"Билет\"\r\n}";

            // Act
            string jsonRes = JsonSerializer.Serialize(Field, options);

            // Assert
            jsonRes.Should().Be(expectedResult);
        }
    }
}
