﻿using Common.Models.OutputModels;
using FluentAssertions;
using NUnit.Framework;
using System.Text.Encodings.Web;
using System.Text.Json;
using System.Text.Unicode;

namespace Tests.SerializationTest
{
    [TestFixture]
    public class CategoryOutput_JsonSerialize_Tests
    {
        private JsonSerializerOptions options;

        [SetUp]
        public void CreateOptions()
        {
            options = new JsonSerializerOptions
            {
                Encoder = JavaScriptEncoder.Create(UnicodeRanges.BasicLatin, UnicodeRanges.Cyrillic, UnicodeRanges.Armenian),
                WriteIndented = true
            };
        }

        [TestCase]
        public void CategorySerialization_WithoutFields()
        {
            // Arrange
            CategoryOutputModel categoryOutputModel = new CategoryOutputModel()
            {
                Id = 4,
                Name = "test"
            };
            string expectedResult = "{\"Id\":4,\"Name\":\"test\",\"FieldsNames\":null}";

            // Act
            string jsonRes = JsonSerializer.Serialize(categoryOutputModel);

            // Assert
            jsonRes.Should().Be(expectedResult);
        }

        [TestCase]
        public void CategorySerialization_WithFields()
        {
            // Arrange
            CategoryOutputModel categoryOutputModel = new CategoryOutputModel()
            {
                Id = 4,
                Name = "test",
                Fields = new Dictionary<int, string>(
                new List<KeyValuePair<int, string>>() {
                    new KeyValuePair<int, string>(2, "Car"),

                })
            };
            string expectedResult = "{\"Id\":4,\"Name\":\"test\",\"FieldsNames\":{\"2\":\"Car\"}}";

            // Act
            string jsonRes = JsonSerializer.Serialize(categoryOutputModel);

            // Assert
            jsonRes.Should().Be(expectedResult);
        }

        [TestCase]
        public void CategorySerialization_WithManyFields()
        {
            // Arrange
            CategoryOutputModel categoryOutputModel = new CategoryOutputModel()
            {
                Id = 4,
                Name = "test",
                Fields = new Dictionary<int, string>(
                new List<KeyValuePair<int, string>>() {
                    new KeyValuePair<int, string>(2, "Car"),
                    new KeyValuePair<int, string>(3, "Houses"),
                    new KeyValuePair<int, string>(1, "Plaints")
                })
            };
            string expectedResult = "{\"Id\":4,\"Name\":\"test\",\"FieldsNames\":" +
                "{\"2\":\"Car\"," +
                "\"3\":\"Houses\"," +
                "\"1\":\"Plaints\"}}";

            // Act
            string jsonRes = JsonSerializer.Serialize(categoryOutputModel);

            // Assert
            jsonRes.Should().Be(expectedResult);
        }

        [TestCase]
        public void CategorySerialization_WithRussianFields()
        {
            // Arrange
            CategoryOutputModel categoryOutputModel = new CategoryOutputModel()
            {
                Id = 4,
                Name = "test",
                Fields = new Dictionary<int, string>(
                new List<KeyValuePair<int, string>>() {
                    new KeyValuePair<int, string>(2, "Машина")
                })
            };
            string expectedResult = "{\"Id\":4,\"Name\":\"test\",\"FieldsNames\":{\"2\":\"\\u041C\\u0430\\u0448\\u0438\\u043D\\u0430\"}}";

            // Консольно выводит нормально
            Console.WriteLine(expectedResult);

            // Act
            string jsonRes = JsonSerializer.Serialize(categoryOutputModel);

            // Assert
            jsonRes.Should().Be(expectedResult);
        }

        [TestCase]
        public void CategorySerialization_WithRussianFields_CustomOptions()
        {
            // Arrange
            CategoryOutputModel categoryOutputModel = new CategoryOutputModel()
            {
                Id = 4,
                Name = "test",
                Fields = new Dictionary<int, string>(
                new List<KeyValuePair<int, string>>() {
                    new KeyValuePair<int, string>(2, "Машина")
                })
            };
            string expectedResult = "{\r\n  \"Id\": 4,\r\n  \"Name\": \"test\",\r\n  \"FieldsNames\": {\r\n    \"2\": \"Машина\"\r\n  }\r\n}";

            // Act
            string jsonRes = JsonSerializer.Serialize(categoryOutputModel, options);

            // Assert
            jsonRes.Should().Be(expectedResult);
        }

    }
}
