﻿using Common.Models.OutputModels;
using FluentAssertions;
using NUnit.Framework;
using System.Text.Encodings.Web;
using System.Text.Json;
using System.Text.Unicode;

namespace Tests.SerializationTest
{
    [TestFixture]
    public class ProductOutput_JsonSerialize_Tests
    {
        private JsonSerializerOptions options;

        // Вероятно не обязательно было бы это
        [SetUp]
        public void CreateOptions()
        {
            options = new JsonSerializerOptions
            {
                Encoder = JavaScriptEncoder.Create(UnicodeRanges.BasicLatin, UnicodeRanges.Cyrillic, UnicodeRanges.Armenian),
                WriteIndented = true
            };
        }

        [TestCase]
        public void ProductSerialization_WithoutFields()
        {
            // Arrange
            ProductOutputModel ProductOutputModel = new ProductOutputModel()
            {
                Id = 4,
                Name = "ticket",
                Description = "desc",
                CategoryId = 1,
                Price = 120
            };
            string expectedResult = "{\"Id\":4,\"Name\":\"ticket\",\"Description\":\"desc\",\"Price\":120,\"CategoryId\":1,\"FieldsValues\":null}";

            // Act
            string jsonRes = JsonSerializer.Serialize(ProductOutputModel);

            // Assert
            jsonRes.Should().Be(expectedResult);
        }

        [TestCase]
        public void ProductSerialization_WithFields()
        {
            // Arrange
            ProductOutputModel ProductOutputModel = new ProductOutputModel()
            {
                Id = 4,
                Name = "ticket",
                Description = "desc",
                CategoryId = 1,
                Price = 120,
                Fields = new Dictionary<string, string>(new List<KeyValuePair<string, string>>()
                {
                    new KeyValuePair<string, string>("Model", "X7")
                })
            };
            string expectedResult = "{\"Id\":4,\"Name\":\"ticket\",\"Description\":\"desc\",\"Price\":120,\"CategoryId\":1,\"FieldsValues\":{\"Model\":\"X7\"}}";

            // Act
            string jsonRes = JsonSerializer.Serialize(ProductOutputModel);

            // Assert
            jsonRes.Should().Be(expectedResult);
        }

        [TestCase]
        public void ProductSerialization_WithManyFields()
        {
            // Arrange
            ProductOutputModel ProductOutputModel = new ProductOutputModel()
            {
                Id = 4,
                Name = "ticket",
                Description = "desc",
                CategoryId = 1,
                Price = 120,
                Fields = new Dictionary<string, string>(new List<KeyValuePair<string, string>>()
                {
                    new KeyValuePair<string, string>("Model", "X7"),
                    new KeyValuePair<string, string>("Oil", "Disel")
                })
            };
            string expectedResult = "{\"Id\":4,\"Name\":\"ticket\",\"Description\":\"desc\",\"Price\":120,\"CategoryId\":1,\"FieldsValues\":{\"Model\":\"X7\",\"Oil\":\"Disel\"}}";

            // Act
            string jsonRes = JsonSerializer.Serialize(ProductOutputModel);

            // Assert
            jsonRes.Should().Be(expectedResult);
        }

        [TestCase]
        public void ProductSerialization_WithRussianFields()
        {
            // Arrange
            ProductOutputModel ProductOutputModel = new ProductOutputModel()
            {
                Id = 4,
                Name = "Электронный билет",
                Description = "Концерт в Erevan MALL",
                CategoryId = 1,
                Price = 120
            };
            string expectedResult = "{\"Id\":4,\"Name\":\"\\u042D\\u043B\\u0435\\u043A\\u0442\\u0440\\u043E\\u043D\\u043D\\u044B\\u0439 \\u0431\\u0438\\u043B\\u0435\\u0442\",\"Description\":\"\\u041A\\u043E\\u043D\\u0446\\u0435\\u0440\\u0442 \\u0432 Erevan MALL\",\"Price\":120,\"CategoryId\":1,\"FieldsValues\":null}";

            Console.WriteLine(expectedResult);

            // Act
            string jsonRes = JsonSerializer.Serialize(ProductOutputModel);

            // Assert
            jsonRes.Should().Be(expectedResult);
        }

        [TestCase]
        public void ProductSerialization_WithRussianFields_CustomOptions()
        {
            // Arrange
            ProductOutputModel ProductOutputModel = new ProductOutputModel()
            {
                Id = 4,
                Name = "Электронный билет",
                Description = "Концерт в Erevan MALL",
                CategoryId = 1,
                Price = 120
            };
            string expectedResult = "{\r\n  \"Id\": 4,\r\n  \"Name\": \"Электронный билет\",\r\n  \"Description\": \"Концерт в Erevan MALL\",\r\n  \"Price\": 120,\r\n  \"CategoryId\": 1,\r\n  \"FieldsValues\": null\r\n}";

            Console.WriteLine(expectedResult);

            // Act
            string jsonRes = JsonSerializer.Serialize(ProductOutputModel, options);

            // Assert
            jsonRes.Should().Be(expectedResult);
        }
    }
}
