using NUnit.Framework;
using FluentAssertions;

namespace Tests
{
    [TestFixture]
    public class SimpleUnitTests
    {
        [TestCase]
        public void Fluent_Summ_Test()
        {
            // Arrange
            int a = 5, b = 4;
            int expectedValue = 9;

            // Act
            var res = a + b;

            // Assert
            res.Should().Be(expectedValue);
        }
    }
}