﻿using Common.Methods;
using DbConnections.Models;

namespace DbConnections.Cache
{
    public class FieldCache : IFieldCache
    {
        private Dictionary<string, Field> _fields = new Dictionary<string, Field>();

        public FieldCache() {
            _fields = new Dictionary<string, Field>();
        }        

        public Field? GetField(string name)
        {
            var tmpName = name.BaseFirstHigh();
            return _fields.ContainsKey(tmpName)
                ? _fields[tmpName]
                : null;
        }

        public void AddField(string name, Field field)
        {
            _fields.Add(name.BaseFirstHigh(), field);
        }

        public void RemoveField(string name)
        {
            _fields.Remove(name.BaseFirstHigh());
        }

        public void SetupCache(List<Field> cache)
        {
            foreach (var f in cache)
            {
                _fields[f.Name.BaseFirstHigh()] = f;
            }
        }
    }
}
