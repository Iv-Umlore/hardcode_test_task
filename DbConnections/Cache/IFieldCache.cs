﻿using Common.Methods;
using DbConnections.Models;

namespace DbConnections.Cache
{
    public interface IFieldCache
    {
        public void SetupCache(List<Field> fields);

        public Field? GetField(string name);

        public void AddField(string name, Field field);

        public void RemoveField(string name);
    }
}
