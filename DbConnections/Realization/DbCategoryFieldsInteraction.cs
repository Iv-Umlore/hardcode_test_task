﻿using Common.Models.DbCompositeModels;
using DbConnections.Interfaces;
using DbConnections.Models;

namespace DbConnections.Realization
{
    public class DbCategoryFieldsInteraction : IDbCategoryFieldsInteraction
    {
        private DbTablesContext _context;

        public DbCategoryFieldsInteraction(DbTablesContext dbContext) {
            _context = dbContext;
        }

        public void AddFieldsToCategory(int categoryId, List<Field> fields)
        {
            _context.InsertCategoryFields(categoryId, fields.Select(it => it.Id).ToList());
        }

        public void AddFieldToCategory(int fieldId, int categoryId)
        {
            _context.InsertCategoryField(new CategoryFieldRelation() { FieldId = fieldId, CategoryId = categoryId });
        }

        public CategoryFull GetCategoryFieldRelations(int categoryId)
        {
            Category category = _context.Categories.First(it=>it.Id == categoryId);

            var baseQuery = _context.CategoryFieldRelationships.Where(it => it.CategoryId == categoryId);
            // Если у категории нет привязанных полей
            if (baseQuery.Count() == 0)
                return new CategoryFull()
                {
                    Id = null,
                    Category = category,
                    Fields = new List<Field>()
                };

            var catRelatedId = baseQuery.First(it => it.CategoryId == categoryId).Id;

            var relatedIds = baseQuery.Select(it => it.FieldId).ToList();

            List<Field> fields = _context.Fields.Where(it => relatedIds.Contains(it.Id)).ToList();

            return new CategoryFull()
            {
                Id = catRelatedId,
                Category = category,
                Fields = fields
            };
        }

        public void RemoveFieldFromCategory(int relationId)
        {
            _context.DeleteCategoryField(relationId);
        }
    }
}
