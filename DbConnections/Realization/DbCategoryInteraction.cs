﻿using DbConnections.Interfaces;
using DbConnections.Models;

namespace DbConnections.Realization
{
    public class DbCategoryInteraction : IDbCategoryInteraction
    {
        private DbTablesContext _context;

        public DbCategoryInteraction(DbTablesContext dbContext)
        {
            _context = dbContext;
        }

        public int AddCategory(string categoryName)
        {
            return _context.InsertCategory(new Category()
            {
                Name = categoryName
            });
        }

        public IQueryable<Category> GetCategories()
        {
            return _context.Categories;
        }

        public Category GetCategory(int id)
        {
            return _context.Categories.First(c => c.Id == id);
        }

        public void RemoveCategory(int categoryId)
        {
            _context.DeleteCategory(categoryId);
        }
    }
}
