﻿using DbConnections.Interfaces;
using DbConnections.Models;
using SQLite;

namespace DbConnections.Realization
{
    public class DbTables : DbTablesContext
    {
        public DbTables(SQLiteConnectionString connectionString) : base(connectionString)
        {
            _connection.CreateTable<Category>();
            _connection.CreateTable<Product>();
            _connection.CreateTable<Field>();
            _connection.CreateTable<CategoryFieldRelation>();
            _connection.CreateTable<ProductField>();
        }
    }
}
