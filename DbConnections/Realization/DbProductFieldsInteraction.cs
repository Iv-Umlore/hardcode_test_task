﻿using Common.Models.DbCompositeModels;
using DbConnections.Interfaces;
using DbConnections.Models;

namespace DbConnections.Realization
{
    public class DbProductFieldsInteraction : IDbProductFieldsInteraction
    {
        private DbTablesContext _context;

        public DbProductFieldsInteraction(DbTablesContext dbContext)
        {
            _context = dbContext;
        }

        public void AddProductField(int productId, int filedId, string value)
        {
            _context.InsertProductField(new ProductField()
            {
                ProductId = productId,
                FieldId = filedId,
                Value = value
            });
        }

        public ProductFull GetProductFields(int productId)
        {
            var relations = _context.ProductFields.Where(it => it.ProductId == productId).ToList();
            Product product = _context.Products.First(it => it.Id == productId);

            if (product == null)
                throw new NullReferenceException($"Не удалось найти товар по Id {productId}");

            List<FieldValue> fields = new List<FieldValue>();

            foreach (var field in relations)
            {
                var currF = _context.Fields.First(it=>it.Id == field.FieldId);
                if (currF != null)
                {
                    fields.Add(new FieldValue()
                    {
                        Id = field.Id,
                        Field = currF,
                        Value = field.Value
                    });
                }
            }

            return new ProductFull()
            {
                Fields = fields,
                Product = product
            };
        }

        public IQueryable<ProductFull> GetProductFieldsByCategory(int categoryFieldId)
        {
            List<ProductFull> productsFull = new List<ProductFull>();

            var products = _context.Products.Where(it => it.CategoryId == categoryFieldId);

            foreach(var product in products)
            {
                var fieldsRealtion = _context.ProductFields.Where(it=>it.ProductId == product.Id)
                    .Join(_context.Fields,
                    rel => rel.FieldId,
                    fld => fld.Id,
                    (rel, fld) => new FieldValue()
                    {
                        Field = fld,
                        Id = rel.Id,
                        Value = rel.Value
                    }).ToList();

                productsFull.Add(new ProductFull()
                {
                    Fields = fieldsRealtion,
                    Product = product
                });

            }
            return productsFull.AsQueryable();
        }

        public void RemoveProductField(int productFieldId)
        {
            _context.DeleteProductField(productFieldId);
        }
    }
}
