﻿using Common.Methods;
using DbConnections.Cache;
using DbConnections.Interfaces;
using DbConnections.Models;

namespace DbConnections.Realization
{
    public class DbFieldsInteraction : IDbFieldsInteraction
    {
        private DbTablesContext _context;
        private IFieldCache _fieldCache;

        public DbFieldsInteraction(DbTablesContext dbContext, IFieldCache fieldCache)
        {
            _context = dbContext;
            _fieldCache = fieldCache;
            _fieldCache.SetupCache(_context.Fields.ToList());
        }

        public void AddField(string fieldName)
        {
            _context.InsertField(new Field() { Name = fieldName.BaseFirstHigh() });
            var field = _context.Fields.First(it => it.Name == fieldName.BaseFirstHigh());
            _fieldCache.AddField(field.Name, field);
        }

        public Field? GetFieldByName(string fieldName)
        {
            var field = _fieldCache.GetField(fieldName.BaseFirstHigh());
            if (field != null) 
                return field;

            field = _context.Fields.FirstOrDefault(it => it.Name == fieldName.BaseFirstHigh());
            return field;
        }

        public IQueryable<Field> GetFields()
        {
            return _context.Fields;
        }

        public void RemoveField(int fieldID)
        {
            var deletedName = _context.DeleteField(fieldID);
            if (deletedName != null)
                _fieldCache.RemoveField(deletedName);
        }
    }
}
