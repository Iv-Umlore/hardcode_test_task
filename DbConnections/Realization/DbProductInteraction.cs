﻿using DbConnections.Interfaces;
using DbConnections.Models;

namespace DbConnections.Realization
{
    public class DbProductInteraction : IDbProductInteraction
    {
        private DbTablesContext _context;

        public DbProductInteraction(DbTablesContext dbContext)
        {
            _context = dbContext;
        }

        public int AddProduct(Product product)
        {
            return _context.InsertProduct(product);
        }

        public Product GetProductById(int productId)
        {
            return _context.Products.First(it => it.Id == productId);
        }

        public IQueryable<Product> GetProducts()
        {
            return _context.Products;
        }

        public void RemoveProduct(int productId)
        {
            _context.DeleteProduct(productId);
        }
    }
}
