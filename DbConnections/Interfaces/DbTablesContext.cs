﻿using Common.Methods;
using DbConnections.Models;
using SQLite;
using System.Runtime.CompilerServices;

[assembly: InternalsVisibleTo("Tests")]
namespace DbConnections.Interfaces
{

    public abstract class DbTablesContext
    {

        protected SQLiteConnection _connection;

        public DbTablesContext(SQLiteConnectionString connectionString)
        {
            _connection = new SQLiteConnection(connectionString);
        }

        internal void CloseConnection()
        {
            _connection.Close();
            GC.Collect();
            GC.WaitForPendingFinalizers();
        }

        #region GetData

        /// <summary>
        /// Доступ к таблице категорий
        /// </summary>
        internal IQueryable<Category> Categories => _connection.Table<Category>().AsQueryable();

        /// <summary>
        /// Доступ к таблице полей
        /// </summary>
        internal IQueryable<Field> Fields => _connection.Table<Field>().AsQueryable();

        /// <summary>
        /// Доступ к таблице товаров
        /// </summary>
        internal IQueryable<Product> Products => _connection.Table<Product>().AsQueryable();

        /// <summary>
        /// Доступ к вспомогательной таблице полей продуктов
        /// </summary>
        internal IQueryable<ProductField> ProductFields => _connection.Table<ProductField>().AsQueryable();

        /// <summary>
        /// Доступ к вспомогательной таблице полей категории
        /// </summary>
        internal IQueryable<CategoryFieldRelation> CategoryFieldRelationships => _connection.Table<CategoryFieldRelation>().AsQueryable();

        #endregion

        #region Insert

        internal int InsertCategory(Category category)
        {
            category.Name = category.Name.BaseFirstHigh();
            _connection.Insert(category);
            return _connection.Table<Category>().Last().Id;
        }

        /// <summary>
        /// Добавить запиь о продукте
        /// </summary>
        /// <returns> Id добавленной записи </returns>
        internal int InsertProduct(Product product)
        {
            _connection.Insert(product);            
            return _connection.Table<Product>().Last().Id;
        }

        internal void InsertField(Field field)
        {
            _connection.Insert(field);
        }

        internal void InsertFields(List<Field> fields)
        {
            foreach (var field in fields)
                field.Name = field.Name.BaseFirstHigh();
            _connection.InsertAll(fields);
        }

        internal void InsertCategoryField(CategoryFieldRelation cfRelation)
        {
            _connection.Insert(cfRelation);
        }

        internal void InsertCategoryFields(int categoryId, List<int> cfRelationIds)
        {
            var alreadyInputIdsList = _connection.Table<CategoryFieldRelation>()
                .Where(it => it.CategoryId == categoryId && cfRelationIds.Contains(it.FieldId)).Select(it => it.FieldId).ToList();
            cfRelationIds.RemoveAll(it => alreadyInputIdsList.Contains(it));
            _connection.InsertAll(cfRelationIds.Select(
                it => new CategoryFieldRelation()
                {
                    CategoryId = categoryId,
                    FieldId = it
                }));
        }

        internal void InsertProductField(ProductField productField)
        {
            _connection.Insert(productField);
        }

        #endregion

        #region Delete

        internal void DeleteCategory(int categoryId)
        {
            _connection.Table<Category>().Delete(it => it.Id == categoryId);
        }

        internal void DeleteProduct(int productId)
        {
            _connection.Table<Product>().Delete(it => it.Id == productId);
        }

        internal string? DeleteField(int fieldId)
        {
            var deletedNode = _connection.Table<Field>().First(f => f.Id == fieldId);
            _connection.Table<Field>().Delete(it => it.Id == fieldId);

            return deletedNode?.Name;
        }

        internal void DeleteCategoryField(int cfRelationID)
        {
            _connection.Table<CategoryFieldRelation>().Delete(it => it.Id == cfRelationID);
        }

        internal void DeleteProductField(int productFieldId)
        {
            _connection.Table<ProductField>().Delete(it => it.Id == productFieldId);
        }

        #endregion

        #region Not using

        //internal void InsertProductFields(List<ProductField> productFields)
        //{
        //    _connection.InsertAll(productFields);
        //}


        //internal void DeleteFields(List<int> fieldIds)
        //{
        //    _connection.Table<Category>().Delete(it => fieldIds.Contains(it.Id));
        //}

        //internal void DeleteProductFields(List<int> productFieldIds)
        //{
        //    _connection.Table<ProductField>().Delete(it => productFieldIds.Contains(it.Id));
        //}

        #endregion
    }
}
