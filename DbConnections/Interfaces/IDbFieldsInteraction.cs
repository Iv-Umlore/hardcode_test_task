﻿using DbConnections.Models;

namespace DbConnections.Interfaces
{
    public interface IDbFieldsInteraction
    {
        /// <summary>
        /// Получить список всех созданных полей
        /// </summary>
        public IQueryable<Field> GetFields();

        /// <summary>
        /// Добавить новое поле
        /// </summary>
        public void AddField(string fieldName);

        /// <summary>
        /// Удалить существующее поле из базы
        /// </summary>
        public void RemoveField(int fieldID);

        /// <summary>
        /// Получить поле по имени, null если отсутствует
        /// </summary>
        public Field? GetFieldByName(string fieldName);
    }
}
