﻿using DbConnections.Models;

namespace DbConnections.Interfaces
{
    public interface IDbProductInteraction
    {
        /// <summary>
        /// Получить список товаров
        /// </summary>
        public IQueryable<Product> GetProducts();

        /// <summary>
        /// Добавить товар
        /// </summary>
        public int AddProduct(Product product);

        /// <summary>
        /// Удалить товар
        /// </summary>
        public void RemoveProduct(int productId);

        /// <summary>
        /// Получить информацию о конкретном продукте
        /// </summary>
        public Product GetProductById(int productId);
    }
}
