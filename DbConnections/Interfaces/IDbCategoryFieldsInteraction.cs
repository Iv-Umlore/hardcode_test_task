﻿using Common.Models.DbCompositeModels;
using DbConnections.Models;

namespace DbConnections.Interfaces
{
    public interface IDbCategoryFieldsInteraction
    {
        /// <summary>
        /// Добавить поле к категории
        /// </summary>
        public void AddFieldToCategory(int fieldId, int categoryId);

        /// <summary>
        /// Добавить поля к категории
        /// </summary>
        public void AddFieldsToCategory(int categoryId, List<Field> fields);

        /// <summary>
        /// Удалить поле у категории
        /// </summary>
        public void RemoveFieldFromCategory(int relationId);

        /// <summary>
        /// Получить список полей категории
        /// </summary>
        public CategoryFull GetCategoryFieldRelations(int categoryId);
    }
}
