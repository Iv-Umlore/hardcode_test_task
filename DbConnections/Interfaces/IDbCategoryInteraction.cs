﻿using DbConnections.Models;

namespace DbConnections.Interfaces
{
    public interface IDbCategoryInteraction
    {

        /// <summary>
        /// Получить список категорий
        /// </summary>
        public IQueryable<Category> GetCategories();

        /// <summary>
        /// Получить категорию по ID
        /// </summary>
        public Category GetCategory(int id);

        /// <summary>
        /// Добавить новую категорию
        /// </summary>
        public int AddCategory(string categoryName);

        /// <summary>
        /// Удалить категорию
        /// </summary>
        public void RemoveCategory(int category);
    }
}
