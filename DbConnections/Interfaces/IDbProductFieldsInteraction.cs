﻿using Common.Models.DbCompositeModels;

namespace DbConnections.Interfaces
{
    public interface IDbProductFieldsInteraction
    {
        /// <summary>
        /// Возвращает категории конкретного продукта
        /// </summary>
        public ProductFull GetProductFields(int productId);

        /// <summary>
        /// Возвращает список полей по привязке к таблице категории-поля
        /// </summary>
        public IQueryable<ProductFull> GetProductFieldsByCategory(int categoryFieldId);

        /// <summary>
        /// Добавить поле к товару
        /// </summary>
        public void AddProductField(int productId, int filedId, string value);

        /// <summary>
        /// Удалить у товара поле
        /// </summary>
        public void RemoveProductField(int productFieldId);
    }
}
