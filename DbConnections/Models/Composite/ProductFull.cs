﻿using DbConnections.Models;

namespace Common.Models.DbCompositeModels
{
    public class ProductFull
    {

        /// <summary>
        /// Соответсвующий продукт
        /// </summary>
        public Product Product { get; set; }

        /// <summary>
        /// Набор полей
        /// </summary>
        public List<FieldValue> Fields { get; set; } = new List<FieldValue>();
    }

    public class FieldValue
    {
        /// <summary>
        /// Id записи в таблице ProductField
        /// </summary>
        public int Id { get; set; }

        /// <summary>
        /// Значение Field
        /// </summary>
        public Field Field { get; set; }

        /// <summary>
        /// Значение Value
        /// </summary>
        public string Value { get; set; }
    }
}
