﻿using DbConnections.Models;

namespace Common.Models.DbCompositeModels
{
    public class CategoryFull
    {
        /// <summary>
        /// Id таблицы CategoryFieldRealted
        /// </summary>
        public int? Id { get; set; }

        /// <summary>
        /// Категория
        /// </summary>
        public Category Category { get; set; }

        /// <summary>
        /// Поля категории
        /// </summary>
        public List<Field> Fields { get; set; } = new List<Field>();
    }
}
