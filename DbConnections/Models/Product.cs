﻿using SQLite;
using System.Diagnostics.CodeAnalysis;

namespace DbConnections.Models
{
    [Table("products")]
    public class Product
    {
        [PrimaryKey, AutoIncrement]
        [Column("id")]
        public int Id { get; set; }

        /// <summary>
        /// Наименование товара
        /// </summary>
        [Column("name")]
        public string Name { get; set; }

        /// <summary>
        /// Описание товара
        /// </summary>
        [Column("description")]
        public string Description { get; set; }

        /// <summary>
        /// Цена товара
        /// </summary>
        [Column("price")]
        public double Price { get; set; }

        // [Column("photo")]
        // public byte[] Photo { get; set; } = { 0x00 };

        /// <summary>
        /// Id категории товара
        /// </summary>
        [Column("CategoryId")]
        public int CategoryId { get; set; }
    }
}
