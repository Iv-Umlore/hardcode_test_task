﻿using SQLite;

namespace DbConnections.Models
{
    [Table("fields")]
    public class Field
    {
        [PrimaryKey, AutoIncrement]
        [Column("id")]
        public int Id { get; set; }

        [Column("name")]
        public string Name { get; set; }
    }
}
