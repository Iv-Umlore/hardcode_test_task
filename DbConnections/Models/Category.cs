﻿using SQLite;

namespace DbConnections.Models
{
    [Table("categories")]
    public class Category
    {
        [PrimaryKey, AutoIncrement]
        [Column("id")]
        public int Id { get; set; }

        /// <summary>
        /// Наименование категории
        /// </summary>
        [Column("name")]
        public string Name { get; set; }
    }
}
