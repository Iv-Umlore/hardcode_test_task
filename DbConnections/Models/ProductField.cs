﻿using SQLite;

namespace DbConnections.Models
{
    [Table("product_fields")]
    public class ProductField
    {
        [PrimaryKey, AutoIncrement]
        [Column("id")]
        public int Id { get; set; }

        /// <summary>
        /// Id товара
        /// </summary>
        [Column("product_id")]
        public int ProductId { get; set; }

        /// <summary>
        /// Id поля из таблицы field
        /// </summary>
        [Column("field_id")]
        public int FieldId { get; set; }

        /// <summary>
        /// Значение дополнительного поля
        /// </summary>
        [Column("value")]
        public string Value { get; set; }
    }
}
