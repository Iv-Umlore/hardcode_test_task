﻿using SQLite;

namespace DbConnections.Models
{
    [Table("category_fields")]
    public class CategoryFieldRelation
    {
        [PrimaryKey, AutoIncrement]
        [Column("id")]
        public int Id { get; set; }

        /// <summary>
        /// Id категории
        /// </summary>
        [Column("category_id")]
        public int CategoryId { get; set; }

        /// <summary>
        /// Id поля для категории
        /// </summary>
        [Column("field_id")]
        public int FieldId { get; set; }
    }
}
