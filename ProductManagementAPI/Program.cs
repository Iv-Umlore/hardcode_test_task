using DbConnections.Cache;
using DbConnections.Interfaces;
using DbConnections.Realization;
using Services.Interfaces;
using Services.Realization;
using SQLite;

namespace ProductManagementAPI
{
    public class Program
    {
        public static void Main(string[] args)
        {
            var builder = WebApplication.CreateBuilder(args);

            // Add services to the container.

            builder.Services.AddControllers();
            // Learn more about configuring Swagger/OpenAPI at https://aka.ms/aspnetcore/swashbuckle
            builder.Services.AddEndpointsApiExplorer();
            builder.Services.AddSwaggerGen();

            ConfigureServices(builder.Services, builder.Configuration);

            var app = builder.Build();

            // Configure the HTTP request pipeline.
            if (app.Environment.IsDevelopment())
            {
                app.UseSwagger();
                app.UseSwaggerUI();
            }

            app.UseMiddleware<LoggingMiddleware>();

            app.UseHttpsRedirection();
            app.UseAuthorization();
            app.MapControllers();
            app.Run();
        }

        public static void ConfigureServices(IServiceCollection services, IConfiguration configuration)
        {
            services.AddSingleton<ICategoryService, CategoryService>();
            services.AddSingleton<IProductService, ProductService>();
            services.AddSingleton<ICommonService, CommonService>();

            services.AddSingleton<DbTablesContext, DbTables>();

            services.AddSingleton<IDbCategoryInteraction, DbCategoryInteraction>();
            services.AddSingleton<IDbProductInteraction, DbProductInteraction>();
            services.AddSingleton<IDbFieldsInteraction, DbFieldsInteraction>();
            services.AddSingleton<IDbCategoryFieldsInteraction, DbCategoryFieldsInteraction>();
            services.AddSingleton<IDbProductFieldsInteraction, DbProductFieldsInteraction>();

            services.AddSingleton<IFieldCache, FieldCache>();

            string connectionString = configuration.GetSection("ConnectionString").Value;
            services.AddTransient((sCollection) => {
                return new SQLiteConnectionString(connectionString); 
            });
        }
    }
}