﻿using Common.Models.OutputModels;
using Microsoft.AspNetCore.Mvc;
using Services.Interfaces;

namespace ProductManagementAPI.Controllers
{
    [Route("[controller]/")]
    public class CommonController : Controller
    {
        private readonly ICommonService _commonServices;

        public CommonController(ICommonService commonServices)
        {
            _commonServices = commonServices;
        }

        // Получить список доступных полей
        [Route("GetAllFields")]
        [HttpGet]
        public ActionResult<List<FieldOutputModel>> GetAllFields()
        {
            return Ok(_commonServices.GetFields());
        }

        // Получить список полей для категории (чтобы заполнить при формировании товара)
        [Route("GetFieldToCategory")]
        [HttpGet]
        public ActionResult<List<FieldOutputModel>> GetAllFields([FromQuery] int categoryId)
        {
            return Ok(_commonServices.GetCategoryField(categoryId));
        }
    }
}
