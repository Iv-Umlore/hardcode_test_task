﻿using Common.Models.InputModels;
using Common.Models.OutputModels;
using Services.Interfaces;
using Microsoft.AspNetCore.Mvc;

namespace ProductManagementAPI.Controllers
{
    [Route("[controller]/")]
    public class ProductController : ControllerBase
    {
        private readonly IProductService _productService;

        public ProductController(IProductService productService)
        {
            _productService = productService;
        }

        // Добавить товар
        [Route("AddProduct")]
        [HttpPost]
        public ActionResult AddProduct([FromBody] ProductInputModel productInputModel)
        {
            _productService.AddProduct(productInputModel);
            return Ok();
        }

        // Удалить товар
        [Route("DeleteProduct")]
        [HttpDelete]
        public ActionResult DeleteProduct([FromQuery] int productId)
        {
            _productService.RemoveProduct(productId);
            return Ok();
        }

        // Товары по фильтру
        [Route("GetProducts")]
        [HttpPost]
        public ActionResult<List<ProductOutputModel>> GetProduts([FromQuery] int? categoryId = null, [FromBody] Dictionary<string, string>? fieldsFilter = null)
        {
            if (categoryId != null)
            {
                var answer = _productService.GetProducts(categoryId.Value,
                    fieldsFilter ?? new Dictionary<string, string>());
                return Ok(answer);
            }
            else
            {
                return Ok(_productService.GetProducts());
            }
        }

        // Выдать конкретный товар
        [Route("GetProductInfo")]
        [HttpPost]
        public ActionResult<ProductOutputModel> GetProductInfo([FromQuery] int productId)
        {
            return Ok(_productService.GetProduct(productId));
        }

    }
}
