using Common.Models.InputModels;
using Common.Models.OutputModels;
using Microsoft.AspNetCore.Mvc;
using Services.Interfaces;

namespace ProductManagementAPI.Controllers
{
    [Route("[controller]/")]
    public class CategoryController : ControllerBase
    {

        private readonly ICategoryService _categoryService;

        public CategoryController(ICategoryService categoryService)
        {
            _categoryService = categoryService;
        }

        // Список категорий
        [HttpGet]
        [Route("GetCategories")]
        public ActionResult<List<CategoryOutputModel>> GetCategories()
        {
            return Ok(_categoryService.GetCategories());
        }

        // Добавить категорию
        [HttpPost]
        [Route("AddCategory")]
        public ActionResult AddCategories([FromBody] CategoryInputModel model)
        {
            _categoryService.AddCategory(model);
            return Ok();
        }

        // Удалить категорию
        [HttpDelete]
        [Route("DeleteCategory")]
        public ActionResult AddCategories([FromQuery] int categoryId)
        {
            _categoryService.RemoveCategory(categoryId);
            return Ok();
        }

        // Информация по конкретной категории
        [HttpPost]
        [Route("GetCategoryInfo")]
        public ActionResult<CategoryOutputModel> GetCategoryInfo([FromQuery] int categoryId)
        {
            return Ok(_categoryService.GetCategory(categoryId));
        }
    }
}