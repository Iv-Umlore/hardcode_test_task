﻿using Common.Methods;
using Common.Models.InputModels;
using Common.Models.OutputModels;
using DbConnections.Interfaces;
using DbConnections.Models;
using Services.Interfaces;

namespace Services.Realization
{
    public class ProductService : IProductService
    {
        private readonly IDbProductInteraction _dbProducts;
        private readonly IDbFieldsInteraction _dbFields;
        private readonly IDbProductFieldsInteraction _dbProductFields;
        private readonly ICommonService _commonServices;

        public ProductService(IDbProductInteraction dbProducts,
            IDbFieldsInteraction dbFields,
            IDbProductFieldsInteraction dbProductFields,
            ICommonService commonServices)
        {
            _dbProducts = dbProducts;
            _dbFields = dbFields;
            _dbProductFields = dbProductFields;
            _commonServices = commonServices;
        }

        public void AddProduct(ProductInputModel productInputModel)
        {
            int productId = _dbProducts.AddProduct( new Product()
            {
                CategoryId = productInputModel.CategoryId,
                Description = productInputModel.Description,
                Name = productInputModel.Name,
                Price = productInputModel.Price
            });

            var correctDict = new Dictionary<string, string>();

            if (productInputModel.Fields != null)
                foreach (var pair in productInputModel.Fields)
                    correctDict.Add(pair.Key.BaseFirstHigh(), pair.Value);

            foreach (var field in correctDict)
            {
                Field? tmpF = _dbFields.GetFieldByName(field.Key);
                if (tmpF == null)
                    throw new ArgumentNullException ("Не удалось получить объект поля по имени");
                _dbProductFields.AddProductField(productId, tmpF.Id, correctDict[tmpF.Name]);
            }
        }

        public ProductOutputModel GetProduct(int id)
        {
            var fullModel =  _dbProductFields.GetProductFields(id);
            Dictionary<string,string> fieldsValue = new Dictionary<string,string>();

            foreach (var field in fullModel.Fields)
            {
                fieldsValue.Add(field.Field.Name, field.Value);
            }
            return new ProductOutputModel()
            {
                Id = fullModel.Product.Id,
                CategoryId = fullModel.Product.CategoryId,
                Description = fullModel.Product.Description,
                Name = fullModel.Product.Name,
                Price = fullModel.Product.Price,
                Fields = fieldsValue
            };
        }

        public List<ProductOutputModel> GetProducts()
        {
            return _dbProducts.GetProducts().Select(it => new ProductOutputModel()
            {
                Id = it.Id,
                CategoryId = it.CategoryId,
                Name = it.Name,
                Price = it.Price,
                Description = it.Description
            }).ToList();
        }

        public List<ProductOutputModel> GetProducts(int categoryIdFilter, Dictionary<string, string> fieldsFilter)
        {
            var tmpQuery = _dbProductFields.GetProductFieldsByCategory(categoryIdFilter);
            foreach(var filter in fieldsFilter)
            {
                tmpQuery = tmpQuery.Where(it => it.Fields.Any(reg => reg.Field.Name == filter.Key && reg.Value == filter.Value));
            }
                
                
            return tmpQuery.Select(it => new ProductOutputModel()
            {
                Id = it.Product.Id,
                CategoryId = it.Product.CategoryId,
                Name = it.Product.Name,
                Price = it.Product.Price,
                Description = it.Product.Description
            }).ToList();
        }

        public void RemoveProduct(int productId)
        {
            var fullModel = _dbProductFields.GetProductFields(productId);

            foreach (var field in fullModel.Fields)
                _dbProductFields.RemoveProductField(field.Id);

            _dbProducts.RemoveProduct(productId);
        }
    }
}
