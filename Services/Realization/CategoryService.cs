﻿using Common.Methods;
using Common.Models.InputModels;
using Common.Models.OutputModels;
using DbConnections.Interfaces;
using DbConnections.Models;
using Services.Interfaces;

namespace Services.Realization
{
    public class CategoryService : ICategoryService
    {
        private readonly IDbCategoryInteraction _dbCategory;
        private readonly IDbFieldsInteraction _dbFields;
        private readonly IDbCategoryFieldsInteraction _dbCategoryFieldsInteraction;
        
        private readonly IProductService _productService;

        public CategoryService(IDbCategoryInteraction dbCategory,
            IDbFieldsInteraction dbFields,
            IDbCategoryFieldsInteraction dbCategoryFieldsInteraction,
            IProductService productService)
        {
            _dbCategory = dbCategory;
            _dbFields = dbFields;
            _dbCategoryFieldsInteraction = dbCategoryFieldsInteraction;
            _productService = productService;
        }

        public void AddCategory(CategoryInputModel inputModel)
        {
            var formatNames = inputModel.Fields?.Select(it => it.BaseFirstHigh()).ToList();
            if (formatNames == null) {
                _dbCategory.AddCategory(inputModel.Name);
                return;
            }

            // Список имен полей в нижнем регистре
            List<string> fieldNamesFormat = _dbFields.GetFields().Select(it=>it.Name).ToList();
            
            // Имена полей, которые ещё не существуют в базе
            var notInsertingFields = formatNames.Where(it => !fieldNamesFormat.Contains(it)).ToList();

            // Добавление полей, которые ещё не существовали
            foreach(var field in notInsertingFields)
            {
                _dbFields.AddField(field);
            }

            int catId = _dbCategory.AddCategory(inputModel.Name);

            // Получение Id полей для вставки (из таблицы Field)
            List<Field> fields = new List<Field>();
            foreach(var fName in formatNames)
            {
                Field? field = _dbFields.GetFieldByName(fName);
                if (field == null) 
                    throw new Exception("Одна из записей не попала в базу");
                fields.Add(field);
            }

            _dbCategoryFieldsInteraction.AddFieldsToCategory(catId, fields);
            // Done
        }

        public List<CategoryOutputModel> GetCategories()
        {
            return _dbCategory.GetCategories()
                .Select(it => new CategoryOutputModel() {
                    Id = it.Id,
                    Name = it.Name 
                }).ToList();
        }

        public CategoryOutputModel GetCategory(int categoryId)
        {
            var category = _dbCategory.GetCategories().Where(it => it.Id == categoryId)
                .Select(it => new CategoryOutputModel()
                {
                    Id = it.Id,
                    Name = it.Name
                }).First();

            var infos = _dbCategoryFieldsInteraction.GetCategoryFieldRelations(categoryId);
            Dictionary<int, string> fieldsDict = new Dictionary<int, string>();

            foreach(var info in infos.Fields)
                fieldsDict.Add(info.Id, info.Name);

            category.Fields = fieldsDict;
            return category;
        }

        public void RemoveCategory(int categoryId)
        {
            List<int>? productForDelete_IDS = _productService.GetProducts(categoryId, new Dictionary<string, string>())
                .Select(it=>it.Id).ToList();

            List<int>? fieldLinkForDelete_IDS = _dbCategoryFieldsInteraction.GetCategoryFieldRelations(categoryId)
                .Fields.Select(it => it.Id).ToList();

            foreach (var prodId in productForDelete_IDS)
                _productService.RemoveProduct(prodId);

            foreach (var catField in fieldLinkForDelete_IDS)
                _dbCategoryFieldsInteraction.RemoveFieldFromCategory(catField);

            _dbCategory.RemoveCategory(categoryId);
            // Done
        }
    }
}
