﻿using Common.Models.OutputModels;
using DbConnections.Interfaces;
using Services.Interfaces;

namespace Services.Realization
{
    public class CommonService : ICommonService
    {
        private readonly IDbFieldsInteraction _dbFields;
        private readonly IDbCategoryFieldsInteraction _dbCategoryFieldsInteraction;
        private readonly IDbProductFieldsInteraction _dbProductFields;

        public CommonService(IDbFieldsInteraction dbFields,
            IDbCategoryFieldsInteraction dbCategoryFieldsInteraction,
            IDbProductFieldsInteraction dbProductFields)
        {
            _dbFields = dbFields;
            _dbCategoryFieldsInteraction = dbCategoryFieldsInteraction;
            _dbProductFields = dbProductFields;
        }

        public FieldOutputModel FindFieldByName(string name)
        {
            var answer = _dbFields.GetFieldByName(name);
            if (answer == null)
                throw new NullReferenceException($"Не удалось найти поле по имени {name}");
            return new FieldOutputModel()
            {
                Id = answer.Id,
                Name = answer.Name
            };
        }

        public List<FieldOutputModel> GetCategoryField(int categoryId)
        {
            return _dbCategoryFieldsInteraction.GetCategoryFieldRelations(categoryId).Fields
                .Select(it => new FieldOutputModel()
                {
                    Id = it.Id,
                    Name = it.Name
                }).ToList();
        }

        public List<FieldOutputModel> GetFields()
        {
            return _dbFields.GetFields()
                .Select(it => new FieldOutputModel() { 
                    Id = it.Id,
                    Name = it.Name 
                }).ToList();
        }

        public List<FieldOutputModel> GetProductField(int productId)
        {
            return _dbProductFields.GetProductFields(productId).Fields.Select(
                it => new FieldOutputModel()
                {
                    Id = it.Field.Id,
                    Name = it.Field.Name
                }).ToList();
        }
    }
}
