﻿using Common.Models.InputModels;
using Common.Models.OutputModels;

namespace Services.Interfaces
{
    public interface ICategoryService
    {
        public List<CategoryOutputModel> GetCategories();

        public CategoryOutputModel GetCategory(int id);

        public void AddCategory(CategoryInputModel inputModel);

        public void RemoveCategory(int categoryId);

    }
}
