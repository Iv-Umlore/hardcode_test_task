﻿using Common.Models.OutputModels;

namespace Services.Interfaces
{
    public interface ICommonService
    {

        public List<FieldOutputModel> GetCategoryField(int categoryId);

        public List<FieldOutputModel> GetProductField(int productId);

        public FieldOutputModel FindFieldByName(string name);

        public List<FieldOutputModel> GetFields();
    }
}
