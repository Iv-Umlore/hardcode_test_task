﻿using Common.Models.InputModels;
using Common.Models.OutputModels;

namespace Services.Interfaces
{
    public interface IProductService
    {
        public List<ProductOutputModel> GetProducts();

        public List<ProductOutputModel> GetProducts(int categoryIdFilter, Dictionary<string, string> fieldsFilter);

        public ProductOutputModel GetProduct(int id);

        /// <summary>
        /// Добавить новый продукт. Fields содержит в себе имя категории и её значение
        /// </summary>
        public void AddProduct(ProductInputModel productInputModel);

        /// <summary>
        /// Удалить продукт
        /// </summary>
        public void RemoveProduct(int productId);
    }
}
