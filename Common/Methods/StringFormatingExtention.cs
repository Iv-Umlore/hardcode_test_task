﻿using System.Text;

namespace Common.Methods
{
    public static class StringFormatingExtention
    {
        /// <summary>
        /// Привести строку к формату Aaaaaaaaa
        /// </summary>
        public static string BaseFirstHigh(this string baseString)
        {
            StringBuilder builder = new StringBuilder();
            builder.Append(baseString.Substring(0, 1).ToUpper());
            builder.Append(baseString.Substring(1).ToLower());

            return builder.ToString();
        }
    }
}
