﻿using System.Text.Json.Serialization;

namespace Common.Models.InputModels
{
    public class CategoryInputModel
    {
        /// <summary>
        /// Наименование категории
        /// </summary>
        [JsonPropertyName("Name")]
        public string Name { get; set; }

        /// <summary>
        /// Имена полей принадлежащих категории
        /// </summary>
        [JsonPropertyName("FieldsNames")]
        public List<string> Fields { get; set; }
    }
}
