﻿using System.Text.Json.Serialization;

namespace Common.Models.OutputModels
{
    public class CategoryOutputModel
    {
        [JsonPropertyName("Id")]
        public int Id { get; set; }

        /// <summary>
        /// Наименование категории
        /// </summary>
        [JsonPropertyName("Name")]
        public string Name { get; set; }

        /// <summary>
        /// FieldID в таблице CategoryFieldsInteraction и имя поля
        /// </summary>
        [JsonPropertyName("FieldsNames")]
        public Dictionary<int, string>? Fields { get; set; }
    }
}
