﻿using System.Text.Json.Serialization;

namespace Common.Models.OutputModels
{
    public class FieldOutputModel
    {
        /// <summary>
        /// Id из таблицы Field
        /// </summary>
        [JsonPropertyName("FieldId")]
        public int Id { get; set; }

        /// <summary>
        /// Имя поля
        /// </summary>
        [JsonPropertyName("Name")]
        public string Name { get; set; }
    }
}
