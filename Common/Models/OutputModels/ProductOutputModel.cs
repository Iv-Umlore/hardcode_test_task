﻿using System.Text.Json.Serialization;

namespace Common.Models.OutputModels
{
    public class ProductOutputModel
    {
        /// <summary>
        /// Id продукта
        /// </summary>
        [JsonPropertyName("Id")]
        public int Id { get; set; }

        /// <summary>
        /// Наименование товара
        /// </summary>
        [JsonPropertyName("Name")]
        public string Name { get; set; }

        /// <summary>
        /// Описание товара
        /// </summary>
        [JsonPropertyName("Description")]
        public string Description { get; set; }

        /// <summary>
        /// Цена товара
        /// </summary>
        [JsonPropertyName("Price")]
        public double Price { get; set; }

        // public byte[] Photo { get; set; } = { 0x00 };

        /// <summary>
        /// Id категории товара
        /// </summary>
        [JsonPropertyName("CategoryId")]
        public int CategoryId { get; set; }

        /// <summary>
        /// Значения вспомогательных полей
        /// </summary>
        [JsonPropertyName("FieldsValues")]
        public Dictionary<string, string> Fields { get; set; }
    }
}
